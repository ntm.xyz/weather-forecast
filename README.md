# Simple Weather Forecast app

## Development

This app require a NodeJS backend as a proxy for https://www.metaweather.com/api. You need to start both BE and FE in development mode

### Install dependencies
Run `npm install`

### Run backend
`npm run start-server`

The BE will be start at http://localhost:8080

### Run frontend
`npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### Run unit tests
`npm test`

Launches the test runner in the interactive watch mode.

## Build Production App
`npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

