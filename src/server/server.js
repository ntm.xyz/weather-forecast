const express = require('express');
const proxy = require('express-http-proxy');
const path = require('path');
const app = express();
app.use(express.static(path.join(__dirname, 'build')));

app.use('/api', proxy('https://www.metaweather.com', {
    proxyReqPathResolver(req) {
        return `/api${req.url}`
    },
    userResHeaderDecorator(headers) {
        headers['Access-Control-Allow-Origin'] = '*';
        return headers;
    }
}));

app.use('/', express.static(path.join(__dirname, '../../build')))

const port = process.env.PORT || 8080;
console.log('[log] App start at port: ', port);
app.listen(port);