import { useState, useEffect } from 'react';

const updaters = [];
let globalState = {};

const setGlobalState = (value) => {
    globalState = { ...globalState, ...value };
    updaters.forEach(updater => {
        updater(globalState);
    });
};

const getGlobalState = () => globalState;

const useGlobalState = (initState) => {
    const [state , updater] = useState({ ...initState, ...globalState });
    globalState = state;

    useEffect(() => {
        updaters.push(updater);
        return () => {
            // remove updater from updaters array
            const updaterIndex = updaters.indexOf(updater);
            updaters.splice(updaterIndex, 1);
        };
    }, []);

    return [state, setGlobalState];
}

export { setGlobalState, getGlobalState };

export default useGlobalState;