import React, {useMemo} from 'react';
import SearchBar from './components/searchBar/searchBar';
import WeatherForecast from './components/weatherForecast/weatherForecast';
import LoadingPanel from './components/loadingPanel/loadingPannel';
import useGlobalState from './customHooks/useGlobalStore/useGlobalState';

import './App.scss';

function App() {
    const [globalState] = useGlobalState({ isLoading: false });
    return (
        <div className="App">
            <SearchBar></SearchBar>
            <WeatherForecast></WeatherForecast>
            {globalState.isLoading && <LoadingPanel></LoadingPanel>}
        </div>
    );
}

export default App;
