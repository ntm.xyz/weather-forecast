import {setGlobalState} from '../customHooks/useGlobalStore/useGlobalState';

const baseUrl = 'http://localhost:8080/api/'

export const fetchJSON = async (path) => {
    setGlobalState({ isLoading: true });
    const response = await fetch(`${baseUrl}${path}`, { mode: 'cors'});
    setGlobalState({ isLoading: false });
    return response.json();
}