import {fetchJSON} from './fetchJSON';

export const searchLocation = async (location) => {
    return fetchJSON(`location/search/?query=${location}`);
}

export const retrieveLocationInfo = async (locationId) => {
    return fetchJSON(`/location/${locationId}/`);
}