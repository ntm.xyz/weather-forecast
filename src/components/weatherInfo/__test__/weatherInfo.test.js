import React from 'react';
import renderer from 'react-test-renderer';

import styles from '../weatherInfo.module.scss';
import WeatherInfo from '../weatherInfo';
import moment from 'moment';

describe('<WeatherInfo>', () => {

    const weatherData = {
        "weather_state_abbr": "hc",
        "applicable_date": "2020-05-08",
        "min_temp": 13.09,
        "max_temp": 23.43,
    };

    let weatherInfo;

    beforeEach(() => {
        weatherInfo = renderer.create(<WeatherInfo weatherData={weatherData} />);
    })

    it('should match snapshot', () => {
        expect(weatherInfo.toJSON()).toMatchSnapshot();
    });

    it('should render friendly day', () => {
        const instance = weatherInfo.root;

        const dateName = moment(weatherData['applicable_date']).calendar({
            sameDay : '[Today]',
            nextDay : '[Tomorrow]',
            nextWeek : 'dddd',
            lastDay: '[Yesterday]',
            lastWeek: '[Last] dddd',
            sameElse : 'L'
        });

        expect(instance.findByProps({ className: styles.weatherInfoDateName }).children).toEqual([dateName]);
    });

    it('should render icon', () => {
        const instance = weatherInfo.root;

        expect(instance.findByProps({ className: styles.weatherInfoIcon }).props.src)
            .toEqual(`https://www.metaweather.com//static/img/weather/${weatherData['weather_state_abbr']}.svg`);
    });

    it('should render min temp', () => {
        const instance = weatherInfo.root;

        expect(instance.findByProps({ className: styles.weatherInfoTempMin }).children.join(''))
            .toEqual(`Min: ${parseInt(weatherData['min_temp'])}°C`);
    });

    it('should render max temp', () => {
        const instance = weatherInfo.root;

        expect(instance.findByProps({ className: styles.weatherInfoTempMax }).children.join(''))
            .toEqual(`Max: ${parseInt(weatherData['max_temp'])}°C`);
    });
})