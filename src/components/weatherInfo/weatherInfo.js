import React from 'react';
import moment from 'moment';

import styles from './weatherInfo.module.scss';

const WeatherInfo = React.memo(( { weatherData } ) => {
    const {
        applicable_date: applicableDate,
        min_temp: minTemp,
        max_temp: maxTemp,
        weather_state_abbr: weatherStateAbbr,
    } = weatherData;
    const dateName = moment(applicableDate).calendar({
        sameDay : '[Today]',
        nextDay : '[Tomorrow]',
        nextWeek : 'dddd',
        lastDay: '[Yesterday]',
        lastWeek: '[Last] dddd',
        sameElse : 'L'
    });
    return (
        <div className={styles.weatherInfo}>
            <div className={styles.weatherInfoDateName}>{dateName}</div>
            <img className={styles.weatherInfoIcon}
                 src={`https://www.metaweather.com//static/img/weather/${weatherStateAbbr}.svg`}/>
            <div className={styles.weatherInfoTempMin} >Min: {parseInt(minTemp)}°C</div>
            <div className={styles.weatherInfoTempMax} >Max: {parseInt(maxTemp)}°C</div>
        </div>
    )
})

export default WeatherInfo;