import React from 'react';

import styles from './cityInfo.module.scss';

const CityInfo = React.memo(({ city = {} }) => {
    const { title } = city;
    return (
      <div className={styles.cityInfo}>{title}</div>
    );
});

export default CityInfo;