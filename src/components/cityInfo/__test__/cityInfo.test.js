import React from 'react';
import renderer from 'react-test-renderer';
import CityInfo from '../cityInfo';

import styles from '../cityInfo.module.scss';

describe('<CityInfo>', () => {

    const city = {
        "title": "London",
        "location_type": "City",
        "woeid": 44418,
        "latt_long": "51.506321,-0.12714"
    };

    let cityInfo;

    beforeEach(() => {
        cityInfo = renderer.create(<CityInfo city={city} />);
    })

    it('should match snapshot', () => {
        expect(cityInfo.toJSON()).toMatchSnapshot();
    });

    it('should render city.title', () => {
        const instance = cityInfo.root;

        expect(instance.findByProps({ className: styles.cityInfo }).children.join(''))
            .toEqual(city.title);
    });
})