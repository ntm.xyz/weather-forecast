import React, {useEffect, useState} from 'react';
import styles from './weatherForecast.module.scss';
import useGlobalState from '../../customHooks/useGlobalStore/useGlobalState';
import {retrieveLocationInfo} from '../../services/metaWeatherService';
import WeatherInfo from '../weatherInfo/weatherInfo';
import CityInfo from '../cityInfo/cityInfo';

const WeatherForecast = React.memo(({ city, onWeatherDataRetrieved }) => {
    const [globalState] = useGlobalState({ location: {} });
    const [locationData, setLocationData] = useState({ consolidated_weather: [] });

    useEffect(() => {
        const locationId = globalState.location.woeid;
        if (!locationId) return;

        const fetchLocationInfo = async () => {
            const locationInfo = await retrieveLocationInfo(locationId);
            setLocationData(locationInfo);
            if (typeof onWeatherDataRetrieved === 'function') {
                onWeatherDataRetrieved(locationInfo);
            }
        }
        fetchLocationInfo();
    }, [globalState.location]);

    return (
        <div className={styles.weatherForecast}>
            <CityInfo
                className={styles.weatherForecastCityInfo}
                city={globalState.location}
            ></CityInfo>
            <div className={styles.weatherForecastBar}>
                { locationData.consolidated_weather.map((weatherData) => (
                    <WeatherInfo
                        className={styles.weatherForecastWeatherInfo}
                        key={weatherData.id}
                        weatherData={weatherData}
                    />
                )) }
            </div>
        </div>
    )
})

export default WeatherForecast;