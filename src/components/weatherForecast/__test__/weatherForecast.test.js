import React from 'react';
import renderer  from 'react-test-renderer';
import sinon from 'sinon';
import WeatherForecast from '../WeatherForecast';
import { setGlobalState } from '../../../customHooks/useGlobalStore/useGlobalState';
import styles from '../WeatherForecast.module.scss';

import * as metaWeatherService from '../../../services/metaWeatherService';

describe('<WeatherForecast>', () => {

    const locationData = {
        "title": "London",
        "location_type": "City",
        "woeid": 44418,
        "latt_long": "51.506321,-0.12714"
    };
    const weatherData = {
        "consolidated_weather": [
            {
                "id": 4964489378136064,
                "weather_state_abbr": "hc",
                "applicable_date": "2020-05-08",
                "min_temp": 13.09,
                "max_temp": 23.43,
            }
        ]
    }

    let weatherForecast;

    beforeEach(() => {
        renderer.act(() => {
            weatherForecast = renderer.create(<WeatherForecast />);
        });
    })

    it('should match snapshot', () => {
        expect(weatherForecast.toJSON()).toMatchSnapshot();
    });

    describe('should render data from globalState', () => {
        const retrieveLocationInfo = sinon.stub(metaWeatherService, 'retrieveLocationInfo');

        beforeEach(() => {
            retrieveLocationInfo.returns(Promise.resolve(weatherData));

            renderer.act(() => {
                setGlobalState({location: locationData});
            });
        });


        it('should fetch locationInfo when globalState change', () => {
            sinon.assert.calledWith(retrieveLocationInfo, locationData.woeid);
        });

        it('should pass valid data to children components', () => {
            const root = weatherForecast.root;
            const cityInfo = root.findByProps({ className: styles.weatherForecastCityInfo });
            const weatherInfos = root.findAllByProps({ className: styles.weatherForecastWeatherInfo });

            expect(cityInfo.props['city']).toEqual(locationData);
            expect(weatherInfos.length).toEqual(weatherData.consolidated_weather.length);
            weatherInfos.forEach((weatherInfo, index) => {
                expect(weatherInfo.props['weatherData']).toEqual(weatherData.consolidated_weather[index]);
            })
        });
    })


})