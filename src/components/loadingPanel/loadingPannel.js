import React from 'react';

import loadingIcon from '../../assets/images/sunny-light.svg';
import styles from './loadingPannel.module.scss';

const LoadingPanel = React.memo(() => {
    return (
      <div className={styles.loadingPanel}>
          <img className={styles.loadingPanelIcon} src={loadingIcon} />
      </div>
    );
});

export default LoadingPanel;