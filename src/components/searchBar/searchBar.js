import React, {useMemo, useState} from 'react';
import style from './searchBar.module.scss';
import useGlobalState from '../../customHooks/useGlobalStore/useGlobalState';
import {searchLocation} from '../../services/metaWeatherService';

const SearchBar = React.memo(({ onSearchStart }) => {
    const [globalState, setGlobalState] = useGlobalState({ location: {} });

    const [city, setCity] = useState(globalState.location.title || '');
    const [error, setError] = useState('');

    const searchInputKeyPressHandler = useMemo(() => async (event) => {
        if (event.key === 'Enter') {
            if (typeof onSearchStart === 'function') {
                onSearchStart(city);
            }
            setError('');
            const location = await searchLocation(city);
            if (location && location.length) {
                setGlobalState({ location: location[0] });
            } else {
                setError('City not found!');
            }
        }
    }, [city, setGlobalState]);

    const searchInputChangeHandler = useMemo(() => (event) => {
        setCity(event.target.value);
    }, [setCity])

    return (
        <div className={style.searchBar}>
            <input
                className={style.searchBarInput}
                placeholder="Input city name then press Enter key"
                value={city}
                onKeyPress={searchInputKeyPressHandler}
                onChange={searchInputChangeHandler}
            />
            {error && <div className={style.searchBarError}>{error}</div>}
        </div>
    )
})

export default SearchBar;