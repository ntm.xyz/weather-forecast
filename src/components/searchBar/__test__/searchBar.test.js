import React from 'react';
import renderer  from 'react-test-renderer';
import sinon from 'sinon';
import waitUntil from 'async-wait-until';
import styles from '../searchBar.module.scss';

import SearchBar from '../searchBar';
import * as metaWeatherService from '../../../services/metaWeatherService';


describe('<SearchBar>', () => {
    let searchBar;

    beforeEach(() => {
        renderer.act(() => {
            searchBar = renderer.create(<SearchBar />);
        });
    })

    it('should match snapshot', () => {
        expect(searchBar.toJSON()).toMatchSnapshot();
    });

    describe('should render properly when inputting data', () => {
        const searchText = 'london';
        const locationData = [
            {
                "title": "London",
                "location_type": "City",
                "woeid": 44418,
                "latt_long": "51.506321,-0.12714"
            }
        ];
        const searchLocation = sinon.stub(metaWeatherService, 'searchLocation');
        let root;
        let input;

        beforeEach(() => {
            root = searchBar.root;
            input = root.findByProps({ className: styles.searchBarInput });

            renderer.act(() => {
                input.props.onChange({
                    target: {
                        value: searchText,
                    }
                });
            });

        });

        it('should fetch location data when data is inputted', () => {
            renderer.act(() => {
                input.props.onKeyPress({
                    key: 'Enter'
                });
            })

            sinon.assert.calledWith(searchLocation, searchText);
        });

        it('should show error if there is empty data ', async () => {
            searchLocation.returns(Promise.resolve([]));

            renderer.act(() => {
                input.props.onKeyPress({
                    key: 'Enter'
                });
            });

            // should wait for the async function finish
            await waitUntil(() => true);

            const searchBarError = root.findAllByProps({ className: styles.searchBarError });
            expect(searchBarError).toHaveLength(1);
        });

        it('should hide error if there is data ', async () => {
            searchLocation.returns(Promise.resolve(locationData));

            renderer.act(() => {
                input.props.onKeyPress({
                    key: 'Enter'
                });
            });

            // should wait for the async function finish
            await waitUntil(() => true);

            const searchBarError = root.findAllByProps({ className: styles.searchBarError });
            expect(searchBarError).toHaveLength(0);
        });
    })



})